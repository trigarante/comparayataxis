import axios from "axios";
import dbService from "./configBase";

const saveDataCliente = {};

saveDataCliente.search = function(data, accessToken) {
  return axios({
    method: "post",
    headers: { Authorization: `Bearer ${accessToken}` },
    url: process.env.promoCore + "/v2/json_quotation/customer_record",
    data: JSON.parse(data),
  });
};
export default saveDataCliente;
