import axios from 'axios'


const newProspectService = {}

newProspectService.nuevoProspecto = function (peticion, accessToken) {

  	return axios({
	    method: "post",
	    headers: {'Authorization': 'Bearer '+accessToken},
	    url: process.env.urlNewCoreCompara + '/v3/cotizaciones/branding',
		data: JSON.parse(peticion)
  	})
}
export default newProspectService
3
