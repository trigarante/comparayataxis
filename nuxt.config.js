module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: "ComparaYa",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      // { hid: 'description', name: 'description', content: 'Template to pages from mejorseguro' }
    ],
    link: [{ rel: "icon", type: "image/png", href: "favicon.png" }],
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3bd600" },
  /*
   ** Build configuration
   */
  build: {
    // analyze:true,
    //vendor: ['jquery', 'bootstrap'],
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/,
        });
      }
    },
  },
  // include bootstrap css
  css: [
    "static/css/bootstrap.min.css",
    "static/css/styles.css",
    "static/css/circle.css",
  ],
  // include bootstrap js on startup
  plugins: [
    { src: "~/plugins/filters.js", ssr: false },
    { src: "~/plugins/apm-rum.js", ssr: false },
  ],

  modules: [
    ["nuxt-validate", { lang: "es" }],
    ["@nuxtjs/google-tag-manager", { id: "GTM-PS23T2X" }],
  ],
  env: {
    // SERVICIOS CATALOGOS
    catalogo: "https://dev.ws-qualitas.com",
    urlNewDataBase: "https://ahorraseguros.mx/servicios",
    urlDB: "https://ahorraseguros.mx/ws-rest/servicios",
    /*PRODUCCION*/
    // urlNewCoreCompara: "https://core-comparaya.com",

    /*PRUEBAS*/
    urlNewCoreCompara: "https://dev.core-comparaya.com",

    Environment: "DEVELOP",

    /**VALIDACIONES */
    urlValidaciones: "https://core-blacklist-service.com/rest",

    /*MONITOREO*/
    /*PRODUCCIÓN*/
    urlMonitoreo: "https://core-monitoreo-service.com/v1",
    hubspot: "https://core-hubspot-dev.mark-43.net/deals/landing",
    promoCore: "https://dev.core-persistance-service.com", //CORE DESCUENTOS Y TELEFONOS
  },
  render: {
    http2: { push: true },
    resourceHints: false,
    compress: { threshold: 9 },
  },
  router: {
    base: "/seguros-para-taxis/",
  },
};
