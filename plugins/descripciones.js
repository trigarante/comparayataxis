import marcasService from './marcas'
import autosService from './ws-autos'

const descripcionesService ={
  url: process.env.catalogo + "/catalogos"
}

descripcionesService.search=function (marca, modelo) {
  return autosService.get(
    process.env.catalogo + `/v3/qualitas-private/models?brand=${marca}&year=${modelo}`
  )
}
export default descripcionesService
