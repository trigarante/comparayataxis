import autosService from './ws-autos'

const modelosService ={
  url: process.env.catalogo + "/catalogos"
}

modelosService.search=function (marca) {
  return autosService.get(
    process.env.catalogo + `/v3/qualitas-private/years?brand=${marca}`
  )
}
export default modelosService
