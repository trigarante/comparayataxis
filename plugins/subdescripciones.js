import autosService from './ws-autos'

const subdescripcionesService ={
  url: process.env.catalogo + "/catalogos"
}

subdescripcionesService.search=function (marca, modelo, submarca) {
  return autosService.get(
    process.env.catalogo + `/v3/qualitas-private/variants?brand=${marca}&year=${modelo}&model=${submarca}`
  );
}
export default subdescripcionesService
