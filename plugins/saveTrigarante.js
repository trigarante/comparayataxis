import axios from 'axios'

const url = process.env.coreBranding

class SaveService {
  saveProspecto(peticion, accessToken) {
    return axios({
      method: "post",
      headers: { Authorization: "Bearer " + accessToken },
      //Branding salesForce
      url: process.env.promoCore + "/v3/cotizaciones/branding",
      data: JSON.parse(peticion),
    });
  }
  saveHubspot(peticion) {
    return axios({
      method: "post",
      headers: { "Content-Type": "application/json" },
      url: process.env.hubspot,
      data: JSON.parse(peticion),
    });
  }
  saveCotizacion(peticion, accessToken, cotizacionAli) {
    return axios({
      method: "put",
      headers: { Authorization: "Bearer " + accessToken },
      //Ali salesForce
      url: process.env.promoCore + `/v1/cotizaciones-ali/${cotizacionAli}`,
      data: JSON.parse(peticion),
    });
  }
  saveLeadEmision(peticion, accessToken) {
    return axios({
      method: "post",
      headers: { Authorization: `Bearer ${accessToken}` },
      //EMISION SALESFORCE
      url: process.env.promoCore + "/v3/issue/request_online",
      data: JSON.parse(peticion),
    });
  }
  saveLeadInteraccion(peticion, accessToken) {
    return axios({
      method: "post",
      headers: { Authorization: `Bearer ${accessToken}` },
      //INTERACCION SALESFORCE
      url: process.env.promoCore + "/v3/interaction/request_online",
      data: JSON.parse(peticion),
    });
  }
  saveDataCliente(peticion, accessToken) {
    return axios({
      method: "post",
      headers: { Authorization: `Bearer ${accessToken}` },
      //LogDatos SALESfORCE
      url: process.env.promoCore + "/v2/json_quotation/customer_record",
      data: JSON.parse(peticion),
    });
  }
}

export default SaveService