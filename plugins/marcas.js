import autosService from './ws-autos'

const marcasService ={
  url: process.env.catalogo + "/catalogos"
}

marcasService.search=function () {
  return autosService.get(
    process.env.catalogo + `/v3/qualitas-private/brands`
  )
}
export default marcasService



