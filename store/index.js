import Vuex from "vuex";
import cotizacion from "~/plugins/cotizacion";
import database from "~/plugins/saveData";
import getTokenService from "../plugins/getToken";
import validacionesService from "../plugins/validaciones";
import dbService from "../plugins/configBase";
import monitoreoService from "../plugins/monitoreo";
import saveBrandingService from "~/plugins/saveBrandingService";
import saveCotizacion from "~/plugins/saveCotizacion";
import telApi from "~/plugins/telefonoService.js";
import cotizacionPromo from "~/plugins/descuentoService.js";
import saveDataCliente from "~/plugins/saveDataCliente";
import SaveService from "~/plugins/saveTrigarante";

const saveService = new SaveService();


const createStore = () => {
  const validacion = new validacionesService();
  return new Vuex.Store({
    state: {
      idCotizacionAli: "",
      ambientePruebas: false,
      cargandocotizacion: false,
      sin_token: false,
      tiempo_minimo: 1.3,
      clickInteraccion: false,
      clickEcommerce: false,
      mostrarBoton: false,
      promo: "Recibe 20% de desc + 10% adicional + 12 MSI.",
      config: {
        loading:true,
        tresCaminos: false,
        aseguradora: "",
        cotizacion: false,
        emision: false,
        descuento: 0,
        telefonoAS: "",
        grupoCallback: "",
        from: "",
        idPagina: 187,
        idMedioDifusion: 2402,
        idCampana: 0,
        idSubRamo: 50,
        habilitarBtnEmision: false,
        habilitarBtnInteraccion: false,
        loading: false,
        urlRedireccion: "",
        idCotizacionNewCore: 0,
        accessToken: "",

        desc: "",
        msi: "",
        promoImg: "",
        promoLabel: "",
        promoSpecial: "false",
      },
      ejecutivo: {
        nombre: "",
        correo: "",
        id: 0,
      },
      formData: {
        urlOrigen: "",
        aseguradora: "",
        marca: "",
        modelo: "",
        descripcion: "",
        detalle: "",
        clave: "",
        cp: "",
        nombre: "",
        telefono: "",
        gclid_field: "",
        correo: "",
        edad: "",
        fechaNacimiento: "",
        genero: "",
        emailValid: "",
        telefonoValid: "",
        codigoPostalValid: "",
        idLogData: "",
        idHubspot: "",
      },
      cotizacion: {},
    },
    actions: {
      getToken() {
        return new Promise((resolve, reject) => {
          getTokenService.search(dbService.tokenData).then(
            (resp) => {
              if (typeof resp.data == "undefined") {
                this.state.config.accessToken = resp.accessToken;
              } else if (typeof resp.data.accessToken != "undefined") {
                this.state.config.accessToken = resp.data.accessToken;
              }
              localStorage.setItem("authToken", this.state.config.accessToken);
              resolve(resp);
            },
            (error) => {
              reject(error);
            }
          );
        });
      },
    },
    mutations: {
      cotizacionPromo: function (state) {
        cotizacionPromo
          .search(state.config.aseguradoraCatalogos)
          .then((resp) => {
            resp = resp.data;
            if (parseInt(resp.discount)) {
              state.config.descuento = resp.discount;
            } else {
              state.config.descuento = 0;
            }
            if (parseInt(resp.delay)) {
              state.config.time = resp.delay;
            } else {
              state.config.time = 5000;
            }
          })
          .catch((error) => {
            state.config.descuento = 0;
            state.config.time = 5000;
          });
      },

      telApi: function (state) {
        try {
          telApi
            .search(state.config.idMedioDifusion)
            .then((resp) => {
              resp = resp.data;
              let telefono = resp.telefono;
              if (parseInt(telefono)) {
                var tel = telefono + "";
                var tel2 = tel.substring(2, tel.length);
                state.config.telefonoAS = tel2;
              }
            })
            .catch((error) => {
              console.log(error);
            });
        } catch (error) {
          console.log(error);
        }
      },
      validarTokenCore: function (state) {
        try {
          if (process.browser) {
            if (localStorage.getItem("authToken") === null) {
              state.sin_token = true;
              console.log("NO HAY TOKEN...");
            } else {
              state.config.accessToken = localStorage.getItem("authToken");
              var tokenSplit = state.config.accessToken.split(".");
              var decodeBytes = atob(tokenSplit[1]);
              var parsead = JSON.parse(decodeBytes);
              var fechaUnix = parsead.exp;
              /*
               * Fecha formateada de unix a fecha normal
               * */
              var expiracion = new Date(fechaUnix * 1000);
              var hoy = new Date(); //fecha actual
              /*
               * Se obtiene el tiempo transcurrido en milisegundos
               * */
              var tiempoTranscurrido = expiracion - hoy;
              /*
               * Se obtienen las horas de diferencia a partir de la conversión de los
               * milisegundos a horas.
               * */
              var horasDiferencia =
                Math.round(
                  (tiempoTranscurrido / 3600000 + Number.EPSILON) * 100
                ) / 100;

              if (hoy > expiracion || horasDiferencia < state.tiempo_minimo) {
                state.sin_token = "expirado";
              }
            }
          }
        } catch (error2) {
          console.log(error2);
        }
      },
      saveHubspot: function (state) {
        let data = {
          mobilephone: state.formData.telefono,
          email: state.formData.correo,
          firstname: state.formData.nombre,
          lastname: "",
          marca: state.formData.marca,
          modelo: state.formData.modelo,
          submarca: state.formData.descripcion,
          socio: "QUALITAS",
        };
        saveService
          .saveHubspot(JSON.stringify(data))
          .then((resp) => {
            state.formData.idHubspot = resp.data.id;
            this.commit("saveDataCliente");
          })
          .catch((error) => {
            console.log("mensaje de error33 ¨*********", error);
            var status = "ERR_CONNECTION_REFUSED";
            if (typeof error.status != "undefined") {
              status = error.status;
            } else if (typeof error.response != "undefined") {
              if (typeof error.response.status != "undefined") {
                status = error.response.status;
              }
            }
            if (status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  if (typeof resp.data == "undefined") {
                    state.config.accessToken = resp.accessToken;
                  } else if (typeof resp.data.accessToken != "undefined") {
                    state.formData.idHubspot = resp.data.message;
                    console.log(
                      "mensaje de error11 ¨*********",
                      state.formData.idHubspot
                    );
                  }
                  this.commit("saveDataCliente");
                })
                .catch((error) => {
                  state.formData.idHubspot = resp.data.message;

                  this.commit("saveDataCliente");
                });
            } else {
              state.formData.idHubspot =
                typeof error.response == "undefined"
                  ? "{}"
                  : error.response.data.message;

              this.commit("saveDataCliente");
            }
          });
      },
      saveDataCliente: function (state) {
        let peticionProspecto = {
          nombreUsr: state.formData.nombre,
          telefonoUsr: state.formData.telefono,
          emailUsr: state.formData.correo,
          generoUsr: state.formData.genero,
          codigoPostalUsr: state.formData.cp,
          edadUsr: state.formData.edad,
          respuestaCot: "{}",
          emailValid: state.formData.emailValid === true ? 1 : 0,
          telefonoValid: state.formData.telefonoValid,
          mensajeSMS: state.config.textoSMS,
          codigoPostalValid: state.formData.codigoPostalValid === true ? 1 : 0,
          idSubRamo: state.config.idSubRamo,
          idMedioDifusion: state.config.idMedioDifusion,
          aseguradora: state.config.aseguradora,
          idPagina: state.config.idPagina,
          telefonoAS: state.config.telefonoAS,
        };
        state.config.dataPeticionCotizacion = peticionProspecto;
        let datosCliente;
        state.formData.telefonoValid = false;
        datosCliente = {
          datosCot: JSON.stringify(state.formData),
          idMedioDifusion: state.config.idMedioDifusion,
          idPagina: state.config.idPagina,
          idSubRamo: state.config.idSubRamo,
          peticionesAli: JSON.stringify({}),
        };
        this.commit("enhancedConversionGTAG");
        saveDataCliente
          .search(JSON.stringify(datosCliente), state.config.accessToken)
          .then((resp) => {
            state.formData.idLogData = resp.data.data.idLogClient;
            this.commit("saveData");
          })
          .catch((error) => {
            var status = "ERR_CONNECTION_REFUSED";
            if (typeof error.status != "undefined") {
              status = error.status;
            } else if (typeof error.response != "undefined") {
              if (typeof error.response.status != "undefined") {
                status = error.response.status;
              }
            }
            if (status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  if (typeof resp.data == "undefined") {
                    state.config.accessToken = resp.accessToken;
                  } else if (typeof resp.data.accessToken != "undefined") {
                    state.config.accessToken = resp.data.accessToken;
                  }
                  this.commit("saveDataCliente");
                })
                .catch((error) => {
                  console.log("Hubo un problema al generar el token");
                  state.idEndPoint = 1;
                  this.catchStatusAndMessage(error, {
                    tokenData: process.env.tokenData,
                  });
                });
            } else {
              state.idEndPoint = 1;
              this.catchStatusAndMessage(error, datosCliente);
            }
          });
        delete state.formData.urlOrigen;
      },
      enhancedConversionGTAG: function (state) {
        dataLayer.push({
          event: "Lead",
          phone: "52" + state.formData.telefono,
          email: state.formData.correo,
        });
      },
      validateData: function (state) {
        setTimeout(() => {
          this.commit("saveHubspot");
        }, state.config.time);
        validacion
          .validarCorreo(state.formData.correo)
          .then((resp) => {
            state.formData.emailValid = resp.data.valido;
            validacion
              .validarTelefono(state.formData.telefono)
              .then((resp) => {
                state.formData.telefonoValid = resp.data.mensaje;
                this.commit("validacionCP");
              })
              .catch((error) => {
                state.formData.telefonoValid = "FIJO";
                state.idEndPoint = 169;
                state.status =
                  error.response !== undefined ? error.response.status : 500;
                state.message =
                  error.response !== undefined
                    ? JSON.stringify({
                        error: error.response.data,
                        peticion: JSON.parse(error.config.data),
                      })
                    : "ERR_CONNECTION_REFUSED";

                this.commit("validacionCP");
              });
          })
          .catch((error) => {
            state.formData.emailValid = true;
            state.idEndPoint = 157;
            state.status =
              error.response !== undefined ? error.response.status : 500;
            state.message =
              error.response !== undefined
                ? JSON.stringify({
                    error: error.response.data,
                    peticion: JSON.parse(error.config.data),
                  })
                : "ERR_CONNECTION_REFUSED";

            validacion
              .validarTelefono(state.formData.telefono)
              .then((resp) => {
                state.formData.telefonoValid = resp.data.mensaje;
                this.commit("validacionCP");
              })
              .catch((error) => {
                state.formData.telefonoValid = "FIJO";
                state.idEndPoint = 169;
                state.status =
                  error.response !== undefined ? error.response.status : 500;
                state.message =
                  error.response !== undefined
                    ? JSON.stringify({
                        error: error.response.data,
                        peticion: JSON.parse(error.config.data),
                      })
                    : "ERR_CONNECTION_REFUSED";

                this.commit("validacionCP");
              });
          });
      },

      validacionCP(state) {
        validacion
          .validarCodigoPostal(
            state.formData.cp,
            state.config.accessToken
          )
          .then((resp) => {
            state.formData.codigoPostalValid =
              resp.data !== "object" && resp.data.length > 0 ? true : false;
            if (state.config.cotizacion) {
              this.commit("cotizar");
            }
          })
          .catch((error) => {
            var status = error.response.status
              ? error.response.status
              : "ERR_CONNECTION_REFUSED";
            state.formData.codigoPostalValid = true;
            if (status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  state.config.accessToken = resp.data.accessToken;
                  this.commit("validacionCP");
                })
                .catch((error) => {
                  if (state.config.cotizacion) {
                    this.commit("cotizar");
                  } else {
                    this.commit("saveData");
                  }
                });
            } else {
              this.commit("valid");
            }
          });
      },
      cotizar: function (state) {
        cotizacion
          .search(
            state.config.aseguradora,
            state.formData.clave,
            state.formData.cp,
            state.formData.descripcion,
            state.config.descuento,
            state.formData.edad,
            (
              "01/01/" +
              (new Date().getFullYear() - state.formData.edad).toString()
            ).toString("yyyyMM/dd"),
            state.formData.genero,
            state.formData.marca,
            state.formData.modelo,
            "cotizacion",
            "AMPLIA",
            "PARTICULAR",
            state.config.accessToken
          )
          .then((resp) => {
            // resp = resp.data;
            state.cotizacion = resp;
            state.cotizacionAli.respuesta = JSON.stringify(resp);
            if (typeof resp != "string") {
              if (
                Object.keys(resp.Cotizacion).includes("PrimaTotal") &&
                resp.Cotizacion.PrimaTotal != null &&
                resp.Cotizacion.PrimaTotal != ""
              ) {
                state.formData.precio = state.cotizacion.Cotizacion.PrimaTotal;
                this.commit("saveData");
                this.commit("limpiarCoberturas");
                this.commit("valid");
              } else {
                state.msj = true;
                this.commit("saveData");
                this.commit("valid");
              }
            } else {
              state.msj = true;
              this.commit("saveData");
              this.commit("valid");
            }
          })
          .catch((error) => {
            var status =
              error.response !== undefined
                ? error.response.status
                : "ERR_CONNECTION_REFUSED";
            if (status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  state.config.accessToken = resp.data.accessToken;
                  this.commit("cotizar");
                  this.commit("valid");
                })
                .catch((error) => {
                  console.log("Hubo un problema al generar el token");
                  var status =
                    error.response !== undefined
                      ? error.response.status
                      : "ERR_CONNECTION_REFUSED";
                  state.idEndPoint = 382;
                  state.status =
                    status !== "ERR_CONNECTION_REFUSED" ? status : 500;
                  state.message =
                    status !== "ERR_CONNECTION_REFUSED"
                      ? JSON.stringify({
                          error: error.response.data,
                          peticion: JSON.parse(error.config.data),
                        })
                      : "ERR_CONNECTION_REFUSED";

                  this.commit("valid");
                });
            } else {
              state.idEndPoint = 304;
              state.status = status !== "ERR_CONNECTION_REFUSED" ? status : 500;
              state.message =
                status !== "ERR_CONNECTION_REFUSED"
                  ? JSON.stringify({
                      error: error.response.data,
                      peticion: JSON.parse(error.config.data),
                    })
                  : "ERR_CONNECTION_REFUSED";
              this.commit("saveData");

              this.commit("valid");
            }
          });
      },

      limpiarCoberturas(state) {
        if (state.cotizacion.Coberturas) {
          for (var c in state.cotizacion.Coberturas[0]) {
            if (state.cotizacion.Coberturas[0][c] === "-") {
              delete state.cotizacion.Coberturas[0][c];
            }
          }
        }
      },
      saveData: function (state) {
        this.commit("valid");
        state.formData.grupoCallback = state.config.grupoCallback;
        state.formData.dominioCorreo = state.config.dominioCorreo;
        var dataGeneral = {
          aseguradora: state.config.aseguradora,
          codigoPostalUsr: state.formData.cp,
          codigoPostalValid: state.formData.codigoPostalValid === true ? 1 : 0,
          datosCot: JSON.stringify(state.formData),
          edadUsr: state.formData.edad,
          emailUsr: state.formData.correo,
          emailValid: state.formData.emailValid === true ? 1 : 0,
          generoUsr: state.formData.genero,
          idMedioDifusion: state.config.idMedioDifusion,
          idPagina: state.config.idPagina,
          idSubRamo: state.config.idSubRamo,
          ip: state.config.ipCliente,
          mensajeSMS: state.config.textoSMS,
          nombreUsr: state.formData.nombre,
          respuestaCot:
            state.config.cotizacion === true
              ? JSON.stringify(state.cotizacion)
              : "{}",
          telefonoAS: state.config.telefonoAS,
          telefonoUsr: state.formData.telefono,
          telefonoValid: state.formData.telefonoValid,
        };
        database
          .search(JSON.stringify(dataGeneral), state.config.accessToken)
          .then((resp) => {
            state.ejecutivo.nombre = resp.data.nombreEjecutivo;
            state.ejecutivo.correo = resp.data.correoEjecutivo;
          })
          .catch((error) => {
            state.status = error.status
              ? error.status
              : error.response
              ? error.response.status
              : 500;
            if (state.status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  if (typeof resp.data == "undefined") {
                    state.config.accessToken = resp.accessToken;
                  } else if (typeof resp.data.accessToken != "undefined") {
                    state.config.accessToken = resp.data.accessToken;
                  }
                  this.commit("saveData");
                })
                .catch((error) => {
                  console.log("Hubo un problema al generar el token");
                  state.status = error.status
                    ? error.status
                    : error.response
                    ? error.response.status
                    : 500;
                  state.idEndPoint = 383;
                  state.message = error.data
                    ? JSON.stringify({
                        error: error.data,
                        peticion: { tokenData: dbService.tokenData },
                      })
                    : JSON.stringify({
                        error: error,
                        peticion: { tokenData: dbService.tokenData },
                      });
                });
            } else {
              state.idEndPoint = 233;
              state.message = error.data
                ? JSON.stringify({
                    error: error.data,
                    peticion: dataGeneral,
                  })
                : JSON.stringify({
                    error: error,
                    peticion: dataGeneral,
                  });
            }
          });
      },

      valid: function (state) {
        state.config.reload = true;
        if (state.formData.precio === "" && state.ejecutivo.id === "") {
          state.msj = true;
        }

        if (
          state.formData.precio === 0.0 ||
          state.formData.precio === "" ||
          state.formData.precio < 1000 ||
          !state.formData.precio
        ) {
          state.msjEje = true;
          state.config.loading = false;
        } else {
          state.cargandocotizacion = true;
        }
      },
    },
  });
};
export default createStore;
