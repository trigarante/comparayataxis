// import autosService from "./ws-autos";
import axios from 'axios'

const cotizacionService = {}

cotizacionService.search = function (data, accessToken) {
  return axios({
    method: "get",
    // COTIZACION SALESFORCE
    // url: 'https://core-persistance-service.com/v2/'+`${aseguradora}`+'/quotation',
    url: process.env.promoCore + `/v2/qualitas/quotation`,
    headers: { Authorization: `Bearer ${accessToken}` },
    params: { request: data },
  });
}
export default cotizacionService


